import pandas as pd
import matplotlib.pyplot as plt
import mplcyberpunk

def linear_regression_analysis(column):
    
    print('Atributo',column)

    y_pts = df[column]
        
    # Gráfico de dispersión
    fig, ax = plt.subplots()
    plt.title(label = column,fontsize=40,color="green")
    ax.scatter(x_pts, y_pts)
    
    # Cálculo de estimadores
    
    y_mean = y_pts.mean()
    
    x_xm = x_pts - x_mean
    y_ym = y_pts - y_mean
    
    sxy = (x_xm * y_ym).sum()
    sxx = ((x_xm)**2).sum()
    syy = ((y_ym)**2).sum()
    print('Sxy:', sxy)
    print('Sxx:', sxx)
    print('Syy:', syy)
    
    b1 = sxy/sxx
    b0 = y_mean - (b1 * x_mean)
    
    print('Recta: Y=', b1, "* x +",b0)    
    
    y1 = b0 + (b1 * x_min)
    y2 = b0 + (b1 * x_max)
    
    print('y(x_min): ', y1)
    print('y(x_max): ', y2)
    
    # Estimación de la varianza y coef. de determinación
    sce = syy - (((sxy)**2)/sxx)
    sigma2 = (sce/(n-2))
    r2 = (1-(sce/syy))
    
    print('SCE:', sce)
    print('sigma2:', sigma2)
    print('R2:', r2)
    
    # Gráficos de dispersión y de la recta
    fig, ax = plt.subplots()
    ax.scatter(x_pts, y_pts)
    ax.plot([x_min, x_max],[y1, y2], "r-")
    
# Programa principal
df = pd.read_csv("ratings.csv")
x_pts = df["quality"]
n = x_pts.count()
x_mean = x_pts.mean()
x_min = x_pts.min()
x_max = x_pts.max()

print('N:', n)
print('x_mean:', x_mean)
print('x_min:', x_min)
print('x_max', x_max)

plt.style.use('cyberpunk')

linear_regression_analysis('helpfulness')
print('\n')
linear_regression_analysis('clarity')
print('\n')
linear_regression_analysis('easiness')
