var ant = "P";
var diff = 999;
console.log("P = ("+ggbApplet.getValue("x(P)")+","+ggbApplet.getValue("y(P)")+","+ggbApplet.getValue("g(x(P),y(P))")+")");
const n = ggbApplet.getValue("N");
console.log("N: "+n);
const tol = ggbApplet.getValue("t");
console.log("tol: "+tol);
const alp = ggbApplet.getValue("a");
console.log("alpha: "+alp);
ggbApplet.evalCommand("Delete(P0)");
console.log("  dx = "+ggbApplet.getValue("gx(x(P),y(P))"));
console.log("  dy = "+ggbApplet.getValue("gy(x(P),y(P))"));
for(var i =0;i<n;i++){
  var curr = "P"+i;
  var vec = "v"+i;
  var grad = "grad"+curr;
  var dist = "dist"+curr+ant;
  var x_ant = "x("+ant+")";
  var y_ant = "y("+ant+")";
  var x = x_ant+"- a gx("+ant+")";
  var y = y_ant+"- a gy("+ant+")";
  ggbApplet.evalCommand(curr+"=("+x+","+y+",g("+x+","+y+"))");
  // console.log("Add "+curr+": "+ggbApplet.getValue(curr));
  ggbApplet.evalCommand(grad+"=Vector((gx("+curr+"),gy("+curr+")))");
  // console.log("Add "+grad+": "+ggbApplet.getValue(grad));
  ggbApplet.evalCommand("SetVisibleInView("+grad+",-1,false)");
  ggbApplet.evalCommand(vec+"=Vector("+curr+",Translate("+curr+",Vector(-a "+grad+")))");
  // console.log("Add "+vec+": "+ggbApplet.getValue(vec));
  ggbApplet.evalCommand(dist+"=Distance("+curr+","+ant+")");
  // console.log("Add "+dist+": "+ggbApplet.getValue(dist));
  diff = ggbApplet.getValue(dist);
  console.log(curr+" = "+"("+ggbApplet.getValue("x("+curr+")")+","+ggbApplet.getValue("y("+curr+")")+","+ggbApplet.getValue("z("+curr+")")+")");
  console.log("  dx = "+ggbApplet.getValue("gx("+x+","+y+")"));
  console.log("  dy = "+ggbApplet.getValue("gy("+x+","+y+")"));
  if (diff < tol) {
    console.log("  dist_ant = "+diff+" < "+tol+" --> SALIR");
    break;
  }
  console.log("  dist_ant = "+diff+" > "+tol);
  console.log("--------------------------------------");
  ant=curr;
}
console.log("########### Pasos: "+(pasos-1)+" ###########");
